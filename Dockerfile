FROM python

RUN apt update

RUN apt install tor -y
COPY conf/torrc /etc/tor/torrc

RUN pip install --upgrade pip
ADD src/requirements.txt /src/requirements.txt
RUN pip install -r /src/requirements.txt

WORKDIR /src/app

CMD ["flask", "run"]
