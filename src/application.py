from flask import Flask

import config


app = Flask(__name__)


from parser.litres import commands


if __name__ == "__main__":
    app.run()
