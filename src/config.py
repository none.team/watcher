import os
import logging
import dotenv


dotenv.load_dotenv('.flaskenv')

logging.getLogger("werkzeug").disabled = True
logging.getLogger('stem').disabled = True

os.makedirs(os.path.dirname(os.getenv("LOG_FILENAME")), exist_ok=True)

logging.basicConfig(
    filename=os.getenv("LOG_FILENAME"),
    format="%(asctime)s - %(levelname)s %(message)s",
    level=logging.INFO
)

TOR_PROXIES = {
    "http": "{}://{}:{}".format(os.getenv("TOR_HTTP_PROTOKOL", "socks5h"),
                                os.getenv("TOR_HTTP_HOST", "localhost"),
                                os.getenv("TOR_HTTP_PORT", 9050)),
    "https": "{}://{}:{}".format(os.getenv("TOR_HTTPS_PROTOKOL", "socks5h"),
                                 os.getenv("TOR_HTTPS_HOST", "localhost"),
                                 os.getenv("TOR_HTTPS_PORT", 9050)),
}

TOR_CONTROL_PORT = int(os.getenv("TOR_CONTROL_PORT", 9051))

DATA_PATH = os.getenv("DATA_PATH", "/src/data/")
