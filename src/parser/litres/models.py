import json, os
from bs4 import BeautifulSoup

import parser
from . import HOST, URL, ANCHOR, PATH


class GenresCollector:
    """ Parser genres from LitRes.ru
        Gives more labels(Y) for books.
    """

    _url = URL["genres"]

    _anchor = ANCHOR["genres"]

    _storage = PATH["data"]

    def __init__(self):
        self._genres = []
        self._errors = dict()

    def __get_genres_root(self):
        print("Root genres loading", end="\r")
        error, text = parser.get(self._url)
        if error is None:
            dom = BeautifulSoup(text, "html.parser")
            for a in [ span.find("a") for span in dom.findAll(*self._anchor["root_genres"]) ]:
                self._genres.append({
                    "link_text": a.text,
                    "link_href": a.get("href")
                })
        else:
            self._errors["source"] = error

    def __get_genres_deep(self, genre, root=False):
        error, text = parser.get(HOST+genre.get("link_href")+("best/" if root else ""))
        if error is None:
            dom = BeautifulSoup(text, "html.parser")
            div = dom.find(*self._anchor["sub_list_wrap"])
            uls = div.findAll("ul")
            if len(uls) < 2 or uls[1].find(*self._anchor["is_done"]):
                return []
            link_chields = []
            for a in uls[1].findAll("a"):
                genre_sub = {
                    "link_text": a.text,
                    "link_href": a.get("href")
                }
                genre_sub["link_chields"] = self.__get_genres_deep(genre_sub)
                link_chields.append(genre_sub)
            return link_chields
        else:
            self._errors["source"] = error

    @property
    def errors(self):
        return self._errors.items()

    def load(self):
        self.__get_genres_root()
        for idx, genre in enumerate(self._genres):
            print("Inner genres loading: [" + ("+" * idx) + "-" * (len(self._genres) - idx - 1) + "]", end="\r")
            self._genres[idx]["link_chields"] = self.__get_genres_deep(genre, True)
        return self

    def save(self):
        if not os.path.exists(self._storage):
            os.makedirs(self._storage)
        with open(self._storage + "genres.json", "w") as file:
            file.write(json.dumps(self._genres))
        return self


class BooksCollector:
    """ Load books major information from genres lists.
        There id, href, title, author and genres for each book.
    """

    _storage = PATH["data"]

    _pagger = URL["list_pagger"]

    _anchor = ANCHOR["lists"]

    def __init__(self):
        self._urls = []
        self._genres = []
        self._errors = dict()
        self.__read_genres()

    def __read_genres(self):
        full_path = self._storage + "genres.json"
        if not os.path.isfile(full_path):
            self._errors["genres"] = "Genres not found."
            return
        with open(full_path) as file:
            self._genres = json.loads(file.read())

    def __parse_genres(self, genres, tags=[]):
        for genre in genres:
            if isinstance(genre, dict):
                if genre.get("link_chields"):
                    tags.append(genre.get("link_text"))
                    self.__parse_genres(genre.get("link_chields"), tags)
                    tags = []
                else:
                    self._urls.append((genre.get("link_href"), tags + [genre.get("link_text")]))

    def __load_list(self, url):
        books = []
        page = 1
        while True:
            error, text = parser.get(HOST + url + self._pagger.format(page))
            if error is None:
                dom = BeautifulSoup(text, "html.parser")
                items = dom.findAll(*self._anchor.get("art-item"))
                if len(items) == 0:
                    break
                for item in items:
                    link = item.find(*self._anchor.get("image-link"))
                    author = item.find(*self._anchor.get("author"))
                    format = item.find(*self._anchor.get("format"))
                    books.append({
                        "id": link.get("data-art"),
                        "href": link.get("href"),
                        "title": link.find("div").get("data-alt"),
                        "author": None if not author else author.text,
                        "format": None if not format else format.text
                    })
                print("{}{}, books count: {}".format(url, page, len(books)), end="\r")
                page += 1
            else:
                self._errors["page"] = error + ", page=" + str(page)
        return books

    def __save(self, full_path, books):
        path = os.path.dirname(full_path)
        if not os.path.exists(path):
            os.makedirs(path)
        with open(full_path, "w") as file:
            file.write(json.dumps(books))

    @property
    def errors(self):
        return self._errors.items()

    def start(self):
        self.__parse_genres(self._genres)
        for url, tags in self._urls:
            full_path = self._storage + "books/" + "/".join([tag.replace("/", "-") for tag in tags]) + "/list.json"
            if not os.path.isfile(full_path):
                books = self.__load_list(url)
                self.__save(full_path, books)
            print(url)
        return self


class CaptchaHandler:
    """ Handler for work with captha page.

        :source HTML source of page
    """

    _anchor = ANCHOR["captcha"]

    def __init__(self, source):
        self._source = source

    @property
    def ip(self):
        dom = BeautifulSoup(self._source, "html.parser")
        node = dom.find(*self._anchor.get("ip"))
        if node is None:
            return None
        return node.get("value")


class BooksCreator:
    """ Load base information(description, pages count, tags, rating,
        comments count and more) about book. Saves item to its genre path
        as JSON file.
    """

    _storage = PATH["data"] + "books/"

    _anchor = ANCHOR["item"]

    def __init__(self):
        self._errors = dict()
        self._lists_paths = []

    @property
    def errors(self):
        return self._errors.items()

    def __load_lists_paths(self):
        for path, _, files in os.walk(self._storage):
            if "list.json" in files:
                self._lists_paths.append(path)

    def __get_ratings(self, dom):
        rating = dict()
        for source in ["litres", "livelib"]:
            div = dom.find(*self._anchor.get("rating_" + source))
            if not div is None:
                data = div.get("data-options")
                for r in ["\n", "\t"]:
                    data = data.replace(r, "")
                data = json.loads(data)
                rating[source] = {
                    "vote": data["mid_vote"],
                    "votes": data["voted"]
                }
        return rating

    def __update(self, item, path):
        error, text = parser.get(HOST + item.get("href"))
        if error is None:
            dom = BeautifulSoup(text, "html.parser")
            ul = dom.find(*self._anchor.get("li_details0")).find("ul")
            item["volume"] = int(ul.find(*self._anchor.get("li_volume")).text.split()[1])
            item["genres"] = [ a.text for a in ul.findAll("li")[1].findAll("a") ]
            tags = ul.find(*self._anchor.get("li_tags"))
            if not tags is None:
                item["tags"] = [ a.text for a in tags.findAll("a") ]
            item["description"] = dom.find(*self._anchor.get("description")).text
            li = dom.find(*self._anchor.get("li_details1")).findAll("li")
            item["age_limit"] = int(li[0].text.split()[-1].replace("+", ""))
            for l in li:
                if "Дата написания" in l.text:
                    item["published_at"] = l.text.replace("Дата написания: ", "")
                    break
            item["rating"] = self.__get_ratings(dom)
        else:
            self._errors["item"] = error + " path={}, id={}".format(path, item.get("id"))
        return item

    def __save(self, item, path):
        with open("{}/{}.json".format(path, item.get("id")), "w") as file:
            file.write(json.dumps(item))

    def start(self):
        self.__load_lists_paths()
        for path in self._lists_paths:
            with open(f"{path}/list.json") as file:
                items = json.loads(file.read())
                for idx, item in enumerate(items or []):
                    if os.path.isfile("{}/{}.json".format(path, item.get("id"))):
                        continue
                    item = self.__update(item, path)
                    self.__save(item, path)
                    print("Books loading: {} of {}".format(idx + 1, len(items)), end="\r")
        return self
