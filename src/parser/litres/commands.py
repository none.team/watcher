from flask.cli import AppGroup

from .models import GenresCollector, BooksCollector, BooksCreator
from application import app


litres = AppGroup("litres", help="LitRes.ru parser commands.")

@litres.command("get-genres")
def litres_get_genres():
    """ Parse available genres and save them.
    """
    m = GenresCollector().load().save()
    for field, message in m.errors:
        app.logger.error(f"{field}: {message}")

@litres.command("get-genres-lists")
def litres_get_genres_lists():
    """ Parse books from genres.
    """
    m = BooksCollector().start()
    for field, message in m.errors:
        app.logger.error(f"{field}: {message}")

@litres.command("get-books-info")
def litres_get_books_info():
    """ Parse base parameters about each book.
    """
    m = BooksCreator().start()
    for field, message in m.errors:
        app.logger.error(f"{field}: {message}")

app.cli.add_command(litres)