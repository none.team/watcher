from config import DATA_PATH


HOST = "https://www.litres.ru"
URL = {
    "genres": HOST + "/pages/new_genres/",
    "list_pagger": "page-{}/?lite=1"
}
ANCHOR = {
    "genres": {
        "root_genres": ("span", {"class": "genre_title"}),
        "sub_list_wrap": ("div", {"class": "sorting-block__dropdown"}),
        "is_done": ("li", {"class": "active"})
    },
    "lists": {
        "art-item": ("div", {"class": "art-item"}),
        "image-link": ("a", {"class": "img-a"}),
        "author": ("a", {"itemprop": "author"}),
        "format": ("a", {"class": "readlink"})
    },
    "captcha": {
        "ip": ("input", {"name": "CaptchaReqIP"})
    },
    "item": {
        "description": ("div", {"itemprop": "description"}),
        "li_details0": ("div", {"class": "biblio_book_info"}),
        "li_details1": ("ul", {"class": "biblio_book_info_detailed_left"}),
        "li_volume": ("li", {"class": "volume"}),
        "li_tags": ("li", {"class": "tags_list"}),
        "rating_litres": ("div", {"class": "rating-source-litres"}),
        "rating_livelib": ("div", {"class": "rating-source-livelib"})
    }
}
PATH = {
    "data": DATA_PATH
}
