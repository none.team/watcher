import requests
from time import sleep
from stem import Signal
from stem.control import Controller

from application import app
from config import TOR_PROXIES, TOR_CONTROL_PORT
from .litres.models import CaptchaHandler


with Controller.from_port(port=TOR_CONTROL_PORT) as controller:
    def __authenticate():
        controller.authenticate()
        controller.signal(Signal.NEWNYM)

def get(url):
    try:
        r = requests.get(url, proxies=TOR_PROXIES)
        if "captcha" in r.text:
            app.logger.info("Captcha for: " + CaptchaHandler(r.text).ip)
            __authenticate()
            return get(url)
        return None, r.text
    except Exception as e:
        return str(e), None